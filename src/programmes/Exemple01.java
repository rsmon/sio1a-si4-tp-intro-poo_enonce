package programmes;

import static leclubdejudo.ClubJudo.getListeDesJudokas;
import static leclubdejudo.Tris.*;
import        leclubdejudo.Judoka;
import static utilitaires.UtilDate.*;


public class Exemple01 {

  public void executer() {
     
    afficherTitre();    
    
    trierLaListeDesJudokas();
    
    for ( Judoka judoka : getListeDesJudokas() ) { traiterJudoka( judoka ); } 
    
    System.out.println();   
  }
 
  void afficherTitre() { System.out.printf("\n Liste des judokas du Club le %-8s\n\n", aujourdhuiChaine()); }
  
  void traiterJudoka( Judoka pJudoka ) {
     
        System.out.printf( " %-10s %-10s %-2s %-10s %4d kg %-20s %2d Victoires\n", 
                           pJudoka.getNom(), pJudoka.getPrenom(), pJudoka.getSexe(),
                           pJudoka.getDateNaissChaine(), pJudoka.getPoids(), pJudoka.getVille(), pJudoka.getNbVictoires()
                         );
   }

  void trierLaListeDesJudokas() { trierParNomPrenom( getListeDesJudokas() );}
}


