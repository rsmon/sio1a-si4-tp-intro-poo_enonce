package programmes;

import static leclubdejudo.ClubJudo.getListeDesJudokas;
import        leclubdejudo.Judoka;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static leclubdejudo.Tris.trierParNomPrenom;

public class Exemple02 {

  public void executer() {
        
    afficherTitre();   
    trierLaListeDesJudokas();
    
    for( Judoka judoka : getListeDesJudokas() ) { traiterJudoka( judoka ); } 
    
    System.out.println();  
  }
  
   void afficherTitre() { System.out.printf("\n Liste des judokas du Club agés de 22 ans au: %10s\n\n", aujourdhuiChaine()); }
 
   void trierLaListeDesJudokas() { trierParNomPrenom(getListeDesJudokas());}
   
   void traiterJudoka(Judoka pJudoka) {
      
        if( pJudoka.getAge() == 22) { 
        
           System.out.printf(" %-10s %-10s %-10s %3dKg %-20s\n", 
                              pJudoka.getPrenom(), 
                              pJudoka.getNom(), 
                              pJudoka.getDateNaissChaine(),
                              pJudoka.getPoids(),
                              pJudoka.getCategorie()
           );
        }
    }  
}


