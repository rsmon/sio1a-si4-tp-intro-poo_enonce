
package programmes;

import         leclubdejudo.Judoka;
import static  leclubdejudo.ClubJudo.getListeDesJudokas;
import static  leclubdejudo.Tris.trierParDateDeNaissance;

public class Exemple03 {

    public void executer() {
        
        System.out.printf("\nListe des judokas à la date du %-12s\n\n",utilitaires.UtilDate.aujourdhuiChaine());
        
        for (Judoka judoka :  trierParDateDeNaissance(getListeDesJudokas())){
        
           judoka.afficher(); System.out.println();      
        }
        
        System.out.println();
    
    }
}
