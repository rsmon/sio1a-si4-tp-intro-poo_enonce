
package programmes;

import static  leclubdejudo.ClubJudo.*;
import static  leclubdejudo.Tris.*;

public class Exemple03b {

    public void executer() {
        
        System.out.printf("\nListe des judokas à la date du %-12s\n\n",utilitaires.UtilDate.aujourdhuiChaine());
         
        afficherListe(trierParDateDeNaissance(getListeDesJudokas()));
       
        System.out.println(); 
    }
}
