package leclubdejudo;

import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.convChaineVersDate;

public class ClubJudo {
    
  private  static List<Judoka> listeDesJudokas = new LinkedList<Judoka>();  
  
  //<editor-fold defaultstate="collapsed" desc="CODE REMPLISSANT LA LISTE Liste Des Judokas ">
  
  static {
      
      listeDesJudokas.add( new Judoka("Durant"    , "Pierre"  , "M", convChaineVersDate("12/05/1983"),  83 , "Arras"          ,  8) );
      listeDesJudokas.add( new Judoka("Martin"    , "Pierre"  , "M", convChaineVersDate("25/11/1991"),  75 , "Lens"           ,  6) );
      listeDesJudokas.add( new Judoka("Lecoutre"  , "Thierry" , "M", convChaineVersDate("05/08/1993"),  72 , "Arras"          ,  5) );
      listeDesJudokas.add( new Judoka("Duchemin"  , "Fabienne", "F", convChaineVersDate("14/03/1992"),  61 , "Lens"           , 10) );
      listeDesJudokas.add( new Judoka("Duchateau" , "Jacques" , "M", convChaineVersDate("18/07/1992"),  91 , "Bapaume"        ,  4) );
      listeDesJudokas.add( new Judoka("Lemortier" , "Laurent" , "M", convChaineVersDate("18/02/1980"),  76 , "Arras"          , 12) );
      listeDesJudokas.add( new Judoka("Dessailles", "Sabine"  , "F", convChaineVersDate("23/03/1990"),  68 , "Arras"          ,  3) );
      listeDesJudokas.add( new Judoka("Bataille"  , "Boris"   , "M", convChaineVersDate("17/11/1987"), 102 , "Vitry-En-Artois",  7) );
      listeDesJudokas.add( new Judoka("Lerouge"   , "Laëtitia", "F", convChaineVersDate("09/10/1992"),  46 , "Lens"           ,  4) );
      listeDesJudokas.add( new Judoka("Renard"    , "Paul "   , "M", convChaineVersDate("16/08/1992"),  68 , "Lens"           ,  9) );
      listeDesJudokas.add( new Judoka("Durant"    , "Jacques" , "M", convChaineVersDate("13/04/1990"),  75 , "Arras"          ,  4) );
      listeDesJudokas.add( new Judoka("Delespaul" , "Martine" , "F", convChaineVersDate("25/02/1991"),  55 , "Lens"           ,  6) );
  }
  
  //</editor-fold>
 
  public static List<Judoka> getListeDesJudokas( ){
        return listeDesJudokas;
  } 
 
  public static void afficherListe( List<Judoka> lj){
  
    for(Judoka j: lj){ j.afficher(); System.out.println();}
  } 
}







