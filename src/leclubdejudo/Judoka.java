
package leclubdejudo;

import java.util.Date;
import static utilitaires.UtilDate.*;
import static leclubdejudo.ReglesCategories.*;

public class Judoka {
    
   private String nom;
   private String prenom;
   private String sexe;
   private Date   dateNaiss;
   private int    poids;
   private String ville;
   private int    nbVictoires;

   public Judoka() {}

   public Judoka(String nom, String prenom, String sexe, Date dateNaiss, int poids, String ville, int nbVictoires) {
        
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.dateNaiss = dateNaiss;
        this.poids = poids;
        this.ville = ville;
        this.nbVictoires = nbVictoires;
   }
  
   public void afficher(){
    
        System.out.printf("%-15s %-15s %-2s %-8s %2d ans %3dKg %-15s %-18s %2d Victoires",
                          nom,
                          prenom,
                          sexe,
                          getDateNaissChaine(),
                          getAge(),
                          poids,
                          getCategorie(),
                          ville,
                          nbVictoires
        );
  
        
     }

    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
   
   public String getNom() {
        return nom;
    }
   
  
   public void setNom(String nom) {
       this.nom = nom;
   }
   
   public String getPrenom() {
       return prenom;
   }
   
   public void setPrenom(String prenom) {
       this.prenom = prenom;
   }
   
   public String getSexe() {
       return sexe;
   }
   
   public void setSexe(String sexe) {
       this.sexe = sexe;
   }
   
   public Date getDateNaiss() {
       return dateNaiss;
   }
   
   public void setDateNaiss(Date dateNaiss) {
       this.dateNaiss = dateNaiss;
   }
   
   public int getPoids() {
       return poids;
   }
   
   public void setPoids(int poids) {
       this.poids = poids;
   }
   
   public String getVille() {
       return ville;
   }
   
   public void setVille(String ville) {
       this.ville = ville;
   }
   
   public int getNbVictoires() {
       return nbVictoires;
   }
   
   public void setNbVictoires(int nbVictoires) {
       this.nbVictoires = nbVictoires;
   }
   
   //</editor-fold>

   public  String  getDateNaissChaine()  { return convDateVersChaine(dateNaiss); }  
     
   public  int     getAge()              { return ageEnAnnees(dateNaiss);}
    
   public  String  getCategorie()        { return determineCategorie(sexe, poids); }
   
   public int      getIndiceCategorie()  { return leclubdejudo.ReglesCategories.determineIndiceCategorie(sexe, poids); }
}

