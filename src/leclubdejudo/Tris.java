package leclubdejudo;

import static java.util.Collections.reverse;
import static java.util.Collections.sort;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Tris {
    
    public static List<Judoka>  trierParNomPrenom(List<Judoka> lj){
          sort(lj, new ComparateurParNomPrenom());
          return lj;
    }
    
    public static  List<Judoka>  trierParSexeNomPrenom(List<Judoka> lj){
          sort(lj, new ComparateurParSexeNomPrenom());
          return lj;
    }
    
    public static  List<Judoka>  trierParPoidsDecroissant(List<Judoka> lj){
          sort(lj, new ComparateurParPoidsDecroissant());
          return lj;
    }
    
    public static  List<Judoka> trierParNom(List<Judoka> lj){
          sort(lj, new ComparateurParNom());
          return lj;
    }
    
    public static List<Judoka>  trierParNbVictoiresDecroissant(List<Judoka> lj){
          
          sort(lj, new ComparateurParNbVictoiresDecroissant());
          reverse(lj);
          return lj;
          
    }  

    
    public static List<Judoka> trierParDateDeNaissance(List<Judoka >  lj){
    
           sort(lj, new ComparateurParDateDeNaissance() );
           return lj;
    
    }
    
//    public static List<Judoka> teteDeListe(List<Judoka> liste, int nb){
//      
//      List<Judoka> resultat= new LinkedList<Judoka>(); 
//      
//      int indMax=nb-1;
//      int i=0;
//      while ( i<=indMax){
//      
//       resultat.add(liste.get(i));i++;
//      }
//      return resultat;
//    }
    
    public static class  ComparateurParNomPrenom implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Judoka j  = (Judoka)  t ;
        Judoka j1 = (Judoka)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom/prénom">
            
            resultat=  j.getNom().compareTo(j1.getNom());
            if (resultat==0) resultat= j.getPrenom().compareTo(j1.getPrenom());
            
        //</editor-fold>
         
        return resultat;  
      }    
    }
    
    public static  class ComparateurParNom implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
        int resultat=0; 
        
         Judoka j  = (Judoka)  t ;
         Judoka j1 = (Judoka)  t1;
            
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom">
            
            resultat=  j.getNom().compareTo(j1.getNom());
            
            //</editor-fold>
        
       
        
        return resultat;  
      }    
    }
   
    public static  class ComparateurParSexeNomPrenom implements Comparator{

     @Override
     public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Judoka j  = (Judoka)  t ;
        Judoka j1 = (Judoka)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par sexe/nom/prénom">
           
        resultat= j.getSexe().compareTo(j1.getSexe());
        if ( resultat==0){
             resultat=  j.getNom().compareTo(j1.getNom());
             if (resultat==0) resultat= j.getPrenom().compareTo(j1.getPrenom());
         }            
        
        //</editor-fold>
      
        return resultat;  
     }    
    }
   
    public static class  ComparateurParPoidsDecroissant implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Judoka j  =  (Judoka)   t ;
          Judoka j1 =  (Judoka)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par poids">
        
           if        ( j.getPoids()   < j1.getPoids() )   resultat =   1;
           else if   ( j.getPoids()   > j1.getPoids() )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
       }    
}
 
    public static class  ComparateurParNbVictoiresDecroissant implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Judoka j  = (Judoka)   t ;
          Judoka j1 = (Judoka)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nombre de victoires">
        
           if       ( j.getNbVictoires()  > j1.getNbVictoires())    resultat =   1;
           else if  ( j.getNbVictoires()  < j1.getNbVictoires() )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
      } 
    } 
    
    
    public static class  ComparateurParDateDeNaissance implements Comparator{

       @Override
       public int compare(Object t, Object t1) {
        
          int resultat=0;
      
           Judoka j  = (Judoka)   t ;
           Judoka j1 = (Judoka)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par date de naissance">
        
            if       ( j.getDateNaiss().before (j1.getDateNaiss()) )   resultat =   1;
            else if  ( j.getDateNaiss().after  (j1.getDateNaiss()) )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
       } 
    
    }
    
}    




