package leclubdejudo;

/**
 *
 * @author rsmon
 */
public class ReglesCategories {
   
  private static String[]  lesNomsDeCategories = { "super-légers","mi-légers","légers","mi-moyens","moyens","mi-lourd","lourds"};

  private static int[][]   limitesCategories   = {
                                                   {60, 66, 73, 81, 90, 100}, // limites de poids  pour les hommes 
                                                   {48, 52, 57, 63, 70,  78}  // limites de poids  pour les femmes 
                                                 };
  
  private static int       indiceSexe( String pSexe) { return pSexe.equals("M") ? 0 : 1 ; }  
   
  public  static String[]  getLesNomsDeCategories() { return lesNomsDeCategories; }
        
  public  static String    determineCategorie(String sexe, int poids){
     
       return lesNomsDeCategories[determineIndiceCategorie(sexe,poids)];       
  }  
   
  public  static int       determineIndiceCategorie(String pSexe, int pPoids){
 
     int i;
  
     int is= indiceSexe(pSexe);

     for(i=0;i<limitesCategories[is].length;i++)
     {         
         if( pPoids<limitesCategories[is][i] ) break;  
            
     }
     
     return i;
  } 
      
  public  static void      afficherCategoriesPourSexe(String pSexe){
       
    int is= indiceSexe(pSexe);  int    indiceMax = lesNomsDeCategories.length-1; 
        
    System.out.printf("%-15s Jusqu'à      %3d  kg\n", lesNomsDeCategories[0], limitesCategories[is][0] );
       
    for (int i=1; i< lesNomsDeCategories.length-1;i++){
                            
      System.out.printf("%-15s de  %d  kg à %3d  kg\n", lesNomsDeCategories[i], limitesCategories[is][i-1], limitesCategories[is][i]); 
    }
      
    System.out.printf("%-15s Au dessus de %3d  kg\n", lesNomsDeCategories[indiceMax], limitesCategories[is][indiceMax-1] ); 
  }
 
}
